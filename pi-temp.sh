#!/bin/sh
# written by Apoc (gitlab.com/ApocArkhesir)

int_cpu_temp=$(cat /sys/class/thermal/thermal_zone0/temp)
cpu_temp=$(echo "$int_cpu_temp / 1000" | bc -l | grep -Po "\d+\.\d{1}")\'C
gpu_temp=$(/usr/bin/vcgencmd measure_temp | grep -Po "\d+\.\d+\'[C]")
echo "$(hostname) | $(date)"
echo "---------------------------------------"
echo "CPU temp: $cpu_temp"
echo "GPU temp: $gpu_temp"
